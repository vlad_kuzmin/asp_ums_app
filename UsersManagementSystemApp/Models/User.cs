﻿using System.ComponentModel.DataAnnotations;

namespace UsersManagementSystemApp.Models
{
    public class UserSingleDto
    {
        [Key]
        public int Id { get; set; }
        
        public string FullName { get; set; }
        
        [EmailAddress]
        public string Email { get; set; }
    }
    
    public class UserCreateDto
    {
        [Required]
        public string FullName { get; set; }
        
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        
        [Required]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Passwords do not match, try again.")]
        public string ConfirmPassword { get; set; }
    }
    
    public class UserUpdateDto
    #nullable enable
    {
        public string? FullName { get; set; }
        
        [EmailAddress]
        public string? Email { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Npgsql;
using UsersManagementSystemApp.Models;

namespace UsersManagementSystemApp.Repositories
{
    public class UserRepository
    /*
     * Данный класс содержит в себе логику операций для пользователей.
     */
    {
        private readonly string _connectionString;

        public UserRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public List<UserSingleDto> GetUsers(string sortBy = null, bool sortAscending = true, string filterBy = null, string filterValue = null)
        /*
         * Данный метод позволяет получить список пользователей,
         * а так же фильтровать и сортировать от большего к меньшему и наоборот.
         */
        {
            using (NpgsqlConnection connection = new NpgsqlConnection(_connectionString))
            {
                string sqlQuery = @"SELECT id, full_name, email FROM users";
                
                // Фильтрация
                if (!string.IsNullOrEmpty(filterBy) && !string.IsNullOrEmpty(filterValue))
                {
                    sqlQuery += $" WHERE {filterBy}=@filterValue";
                }

                // Сортировка
                if (!string.IsNullOrEmpty(sortBy))
                {
                    sqlQuery += $" ORDER BY {sortBy} {(sortAscending ? "ASC" : "DESC")}";
                }
                
                List<UserSingleDto> users = new List<UserSingleDto>();

                using (var command = new NpgsqlCommand(sqlQuery, connection))
                {
                    if (!string.IsNullOrEmpty(filterBy) && !string.IsNullOrEmpty(filterValue))
                    {
                        command.Parameters.AddWithValue("filterValue", filterValue);
                    }
                    
                    connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var user = new UserSingleDto
                            {
                                Id = reader.GetInt32(0),
                                FullName = reader.GetString(1),
                                Email = reader.GetString(2)
                            };
                            users.Add(user);
                        }
                    }
                }

                return users;
            }
        }
        
        public UserSingleDto GetUserById(int id)
        /*
         * Данный метод позволяет получить пользователя по уникальному ключу id,
         * в случае если такой пользователь не был найден возвращает null.
         */
        {
            using (NpgsqlConnection connection = new NpgsqlConnection(_connectionString))
            {
                string sqlQuery = @"
                    BEGIN;
                        SELECT id, full_name, email FROM users WHERE id = @id;
                    COMMIT;
                ";
                
                using (NpgsqlCommand command = new NpgsqlCommand(sqlQuery, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter("@id", id));
                    connection.Open();
                    
                    using (NpgsqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            UserSingleDto user = new UserSingleDto
                            {
                                Id = reader.GetInt32(0),
                                FullName = reader.GetString(1),
                                Email = reader.GetString(2),
                            };
                            return user;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }

        public void CreateUser(UserCreateDto user)
        /*
         * Даннный метод позволяет создать нового пользователя.
         * При успешном завершении операции возвращает true, в противном случае false.
         */
        {
            using (var connection = new NpgsqlConnection(_connectionString))
            {
                connection.Open();

                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {
                        if (UserWithEmailExists(connection, user.Email, transaction))
                        {
                            throw new Exception("User with this email already exists");
                        }

                        string hashedPassword = HashPassword(user.Password);

                        using (var command = new NpgsqlCommand(
                                   "INSERT INTO Users (full_name, email, password) VALUES (@full_name, @email, @password)",
                                   connection))
                        {
                            command.Parameters.AddWithValue("full_name", user.FullName);
                            command.Parameters.AddWithValue("email", user.Email);
                            command.Parameters.AddWithValue("password", hashedPassword);
                            command.Transaction = transaction;

                            command.ExecuteNonQuery();
                        }

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error creating user: " + ex.Message);
                    }
                }
            }
        }

        public void UpdateUser(int id, UserUpdateDto user)
        /*
         * Данный метод позволяет обновить данные кокретного пользователя.
         */
        {
            using (var connection = new NpgsqlConnection(_connectionString))
            {
                connection.Open();

                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {
                        string sqlQuery = "UPDATE users SET";
                        var updateList = new List<string>();
                        
                        if (UserExists(connection, id, transaction))
                        {
                            throw new Exception("User don't exists");
                        }
                        
                        if (!string.IsNullOrEmpty(user.FullName))
                        {
                            updateList.Add("full_name = @full_name");
                        }
                        
                        if (!string.IsNullOrEmpty(user.Email))
                        {
                            if (UserWithEmailExists(connection, user.Email, transaction))
                            {
                                throw new Exception("User with this email already exists");
                            }
                            updateList.Add("email = @email");
                        }

                        if (updateList.Count == 0)
                        {
                            throw new Exception("No fields to update");
                        }
                        
                        sqlQuery += " " + string.Join(", ", updateList) + " WHERE id = @userId";
                        
                        using (var command = new NpgsqlCommand(sqlQuery, connection))
                        {
                            if (user.FullName != null) command.Parameters.AddWithValue("full_name", user.FullName);
                            if (user.Email != null) command.Parameters.AddWithValue("email", user.Email);
                            command.Parameters.AddWithValue("userId", id);
                            command.Transaction = transaction;

                            command.ExecuteNonQuery();
                        }

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error updating user: " + ex.Message);
                    }
                }
            }
        }

        public void DeleteUser(int id)
        /*
         * Данный метод позволяет удалить пользователя из системы.
         */
        {
            using (var connection = new NpgsqlConnection(_connectionString))
            {
                connection.Open();

                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {
                        if (!UserExists(connection, id, transaction))
                        {
                            throw new Exception("User with this id don't exists");
                        }

                        string sqlQuery = "DELETE FROM users WHERE id = @userId";
                        
                        using (var command = new NpgsqlCommand(sqlQuery, connection))
                        {
                            command.Parameters.AddWithValue("userId", id);
                            command.Transaction = transaction;

                            command.ExecuteNonQuery();
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error deleting user: " + ex.Message);
                    }
                }
            }
        }
        
        private bool UserExists(NpgsqlConnection connection, int id, NpgsqlTransaction transaction)
        /*
         * Проверяет существование пользователя по id.
         */
        {
            using (var command = new NpgsqlCommand("SELECT COUNT(*) FROM users WHERE id = @Id", connection))
            {
                command.Parameters.AddWithValue("Id", id);
                command.Transaction = transaction;

                int count = Convert.ToInt32(command.ExecuteScalar());
                return count > 0;
            }
        }

        private bool UserWithEmailExists(NpgsqlConnection connection, string email, NpgsqlTransaction transaction)
        /*
         * Проверяет существование пользователя с email'ом.
         */
        {
            using (var command = new NpgsqlCommand("SELECT COUNT(*) FROM users WHERE email = @Email", connection))
            {
                command.Parameters.AddWithValue("Email", email);
                command.Transaction = transaction;

                int count = Convert.ToInt32(command.ExecuteScalar());
                return count > 0;
            }
        }

        private string HashPassword(string password)
        {
            using (SHA256 sha256 = SHA256.Create())
            {
                byte[] hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(password));
                return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
            }
        }
    }
}
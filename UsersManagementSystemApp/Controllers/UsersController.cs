﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using UsersManagementSystemApp.Models;
using Microsoft.Extensions.Configuration;
using Swashbuckle.AspNetCore.Annotations;
using UsersManagementSystemApp.Repositories;


namespace UsersManagementSystemApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly UserRepository _repository;

        public UsersController(IConfiguration configuration)
        {
            _repository = new UserRepository(configuration.GetConnectionString("Db"));
        }
        
        [HttpGet]
        [SwaggerOperation(Summary = "Get a list of users", Description = "Retrieves a list of users from the database.")]
        [SwaggerResponse(StatusCodes.Status200OK, "The list of users", typeof(IEnumerable<UserSingleDto>))]
        public IActionResult GetUserList(
                [FromQuery][SwaggerParameter("Sort the results by a specific field, e.g. 'full_name'")] string sortBy,
                [FromQuery][SwaggerParameter("True if sorted in ascending order, false if sorted in descending order")] bool sortAscending,
                [FromQuery][SwaggerParameter("Filter the results by a specific field")] string filterBy,
                [FromQuery][SwaggerParameter("The value to use when filtering the results")] string filterValue)
        /*
         * Получить список пользователей.
         * Есть возможность сортировки и фильтрации с помощью query параметров.
         */
        {
            var users = _repository.GetUsers(sortBy, sortAscending, filterBy, filterValue);
            return Ok(users);
        }

        [HttpGet("{id}")]
        [SwaggerOperation(Summary = "Get a concrete user by id", Description = "Retrieves a single user object from the database.")]
        [SwaggerResponse(StatusCodes.Status200OK, "The user", typeof(UserSingleDto))]
        public IActionResult GetUserById(int id)
        /*
         * Получить конкретного пользователя по id.
         */
        {
            var user = _repository.GetUserById(id);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        [HttpPost]
        [SwaggerOperation(Summary = "Create new user", Description = "Create new user in the database.")]
        [SwaggerResponse(StatusCodes.Status200OK, "Create the user", typeof(UserSingleDto))]
        public IActionResult CreateUser(UserCreateDto user)
        /*
         * Создать нового пользователя в системе.
         */
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                _repository.CreateUser(user);
                return Ok(user);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }

        [HttpPatch("{id}")]
        [SwaggerOperation(Summary = "Update user", Description = "Update user in the database.")]
        [SwaggerResponse(StatusCodes.Status200OK, "Update the user", typeof(UserUpdateDto))]
        public IActionResult UpdateUser(int id, [FromBody] UserUpdateDto user)
        /*
         * Обновить пользователя в системе.
         */
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                _repository.UpdateUser(id, user);
                return Ok(user);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }

        [HttpDelete("{id}")]
        [SwaggerOperation(Summary = "Delete user", Description = "Delete user in the database.")]
        [SwaggerResponse(StatusCodes.Status200OK, "Delete the user")]
        public IActionResult DeleteUser(int id)
        /*
         * Удалить пользователя из системы.
         */
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                _repository.DeleteUser(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }
    }
}
﻿using System;
using System.IO;
using Npgsql;

namespace UsersManagementSystemApp.Utils
{
    public class DatabaseInitializer
    /*
     * Класс для инициализации БД. Содержит логику создания самой БД и всех необходимых сущностей используя схему БД.
     */
    {
        private readonly string _connectionString;

        public DatabaseInitializer(string connectionString)
        {
            _connectionString = connectionString;
        }
        
        private void CreateDatabaseIfNotExists()
        /*
         * Данный метод проверяет инициализированна ли БД, если нет то выполняет инициализаию.
         */
        {
            using (var connection = new NpgsqlConnection(_connectionString))
            {
                connection.Open();
                
                // Инициализируем NpgsqlConnectionStringBuilder для извлечения названия базы из ConnectionString.
                NpgsqlConnectionStringBuilder builder = new NpgsqlConnectionStringBuilder(_connectionString);
                string dbName = builder.Database;
                string role = builder.Username;

                // Проверяем существование базы данных.
                string checkDatabaseQuery = $"SELECT 1 FROM pg_database WHERE datname='{dbName}';";
                
                using (var command = new NpgsqlCommand(checkDatabaseQuery, connection))
                {
                    if (command.ExecuteScalar() == null)
                    {
                        // Если база данных не существует, создаем её.
                        string createDatabaseQuery = $"CREATE DATABASE {dbName} OWNER {role};";
                        using (var createCommand = new NpgsqlCommand(createDatabaseQuery, connection))
                        {
                            createCommand.ExecuteNonQuery();
                        }
                    }
                }
            }
        }

        private void ExecuteSchemaScript(string scriptFileName)
        /*
         * Данный метод позволяет создать сущности в БД используя схему.
         */
        {
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string schemaPath = Path.Combine(baseDirectory, scriptFileName);
            
            string script = System.IO.File.ReadAllText(schemaPath);

            using (var connection = new NpgsqlConnection(_connectionString))
            {
                connection.Open();

                using (var command = new NpgsqlCommand(script, connection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        public void Initialize()
        {
            CreateDatabaseIfNotExists();
            ExecuteSchemaScript("Schema.sql");
        }
    }
}